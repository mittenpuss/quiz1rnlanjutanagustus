import React, { useEffect, useState } from 'react'
import { Text, View } from 'react-native'

const Jawaban1 = () =>{

    const [name,setName] = useState('Jhon Doe')

    useEffect(() => {
      setTimeout(() =>{
        setName("Asep")
      },3000)  
        
      }, [name])

      return (
        <View>
            <Text>{name}</Text>
        </View>
      )

}

export default Jawaban1