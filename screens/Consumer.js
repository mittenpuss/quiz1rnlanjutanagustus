import React, { useContext } from 'react'
import {View, Text,FlatList} from 'react-native'
import {RootContext} from './Jawaban2'

const Consumer = () =>{
    
    const state = useContext(RootContext)
 
    console.log(state)

    const renderItem = ({item}) =>{
        return (
            <View>
            <Text style={{paddingTop:20}}>
                {item.name}
            </Text>
            <Text>
                {item.position}
            </Text>
        </View>

        )
    }

    return (
        <View>
              <FlatList
                data={state.name}
                renderItem={renderItem}
            >
            </FlatList>

        </View>
    )
}

export default Consumer