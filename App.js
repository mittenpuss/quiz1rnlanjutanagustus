import React from 'react'
import Jawaban1 from './screens/Jawaban1'
import Jawaban2 from './screens/Jawaban2'

import { View, Text } from 'react-native'

const App = () =>{
  return (
    <View>

        <Text style={{paddingBottom:20}}>
          Quiz 1, Dewa Gede Angga Wijaya
        </Text>

        <Text style={{fontWeight:"bold"}}>
            Jawaban 1:
        </Text>
        <Jawaban1/>

        <Text style={{paddingTop:20,fontWeight:"bold"}}>
            Jawaban 2:
        </Text>
        <Jawaban2/>

    </View>

  )
}

export default App